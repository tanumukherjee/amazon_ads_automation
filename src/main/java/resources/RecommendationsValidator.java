package resources;

import org.testng.Assert;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class RecommendationsValidator {
	
	
	
	public static void createCampaignValidation(JsonObject response) {

		
			System.out.println(response);
			Assert.assertTrue(response.has("status"));
			Assert.assertTrue(response.has("totalResults"));
			System.out.println(response);
			response = response.get("data").getAsJsonObject();
			Assert.assertTrue(response.has("campaign_id"));
			
		}
	

	public static void getCampaignPARValidation(JsonObject response) {
		Gson gson = new Gson();
		response = response.get("data").getAsJsonObject();
		System.out.println(response);
	
	
	
	Assert.assertTrue(response.has("campaign_id"));
	Assert.assertTrue(response.has("campaign_name"));
	Assert.assertTrue(response.has("advertiser"));
	Assert.assertTrue(response.has("frequency_cap"));
	Assert.assertTrue(response.has("campaign_priority"));
	Assert.assertTrue(response.has("external_campaign_id"));
	Assert.assertTrue(response.has("purchase_order_id"));
	Assert.assertTrue(response.has("notes"));
	}
	
	public static void updateCampaignPARValidation(JsonObject response) {

		response = response.get("data").getAsJsonObject();
		System.out.println(response);

		Assert.assertTrue(response.has("campaign_id"));
		Assert.assertTrue(response.has("campaign_name"));
		Assert.assertTrue(response.has("notes"));
		Assert.assertTrue(response.has("campaign_priority"));
		Assert.assertTrue(response.has("frequency_cap"));
		Assert.assertTrue(response.has("external_campaign_id"));
		Assert.assertTrue(response.has("purchase_order_id"));
		Assert.assertTrue(response.has("advertiser"));
		
	}
	public static void createCreativeValidation(JsonObject response) {

		System.out.println(response);
		Assert.assertTrue(response.has("status"));
		Assert.assertTrue(response.has("totalResults"));
		response = response.get("data").getAsJsonObject();
		Assert.assertTrue(response.has("creative_id"));
	}
	public static void getCreativeValidation(JsonObject response) {

		response = response.get("data").getAsJsonObject();
		System.out.println(response);
		Assert.assertTrue(response.has("creative_id"));
		Assert.assertTrue(response.has("creative_name"));
		Assert.assertTrue(response.has("status"));
		Assert.assertTrue(response.has("audio"));
		Assert.assertTrue(response.has("banner_static"));
		Assert.assertTrue(response.has("banner_native"));
		Assert.assertTrue(response.has("external_creative_id"));

		
	}
	public static void updateCreativeValidation(JsonObject response) {
		response = response.get("data").getAsJsonObject();
		System.out.println(response);
		Assert.assertTrue(response.has("creative_id"));
		Assert.assertTrue(response.has("creative_name"));
		Assert.assertTrue(response.has("status"));
		Assert.assertTrue(response.has("audio"));
		Assert.assertTrue(response.has("banner_static"));
		Assert.assertTrue(response.has("banner_native"));
		Assert.assertTrue(response.has("external_creative_id"));

	}
}



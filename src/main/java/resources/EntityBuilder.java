package resources;

public  class EntityBuilder {

	

	String modelType;
	boolean nativeBanner;
	boolean placement;
	boolean staticBanner;
	
	public EntityBuilder setModelType(String modelType) {
		this.modelType = modelType;
		return this;
	}
	public EntityBuilder setNativeBanner(boolean nativeBanner) {
		this.nativeBanner = nativeBanner;
		return this;
	}

	public EntityBuilder setPlacement(boolean placement) {
		this.placement = placement;
		return this;
	}
	public EntityBuilder setStaticBanner(boolean staticBanner) {
		this.staticBanner = staticBanner;
		return this;
	}
	
	public CreativeCreateEntities getEntities() {
		
		return new CreativeCreateEntities(modelType, nativeBanner,placement, staticBanner);
	}
	
	
	
}

package resources;

import java.util.Arrays;

public class CreativeCreateEntities {

	
	String modelType;
	boolean nativeBanner;
	boolean placement;
	boolean staticBanner;
	
	public CreativeCreateEntities(String modelType, boolean nativeBanner, boolean placement,
			boolean staticBanner) {
		
		this.modelType = modelType;
		this.nativeBanner = nativeBanner;
		this.placement = placement;
		this.staticBanner = staticBanner;
	}





	public String getModelType() {
		return modelType;
	}

	public void setModelType(String modelType) {
		this.modelType = modelType;
	}

	public boolean isNativeBanner() {
		return nativeBanner;
	}

	public void setNativeBanner(boolean nativeBanner) {
		this.nativeBanner = nativeBanner;
	}


	public boolean isPlacement() {
		return placement;
	}

	public void setPlacement(boolean placement) {
		this.placement = placement;
	}

	public boolean isStaticBanner() {
		return staticBanner;
	}

	public void setStaticBanner(boolean staticBanner) {
		this.staticBanner = staticBanner;
	}

	
	
	
	@Override
	public String toString() {
		return "CreativeCreateEntities [modelType=" +modelType + ", nativeBanner=" + nativeBanner
				+ ", placement=" + placement + ", staticBanner=" + staticBanner + "]";
	}
	
	
	
	
	
}

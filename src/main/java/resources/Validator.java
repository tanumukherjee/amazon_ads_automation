package resources;

import org.testng.Assert;
import com.google.gson.*;

public class Validator {

	Gson gson = new Gson();

	public static void createCampaignValidation(JsonObject response) {

		System.out.println(response);
		Assert.assertTrue(response.has("status"));
		Assert.assertTrue(response.has("totalResults"));
		System.out.println(response);
		response = response.get("data").getAsJsonObject();
		Assert.assertTrue(response.has("campaign_id"));
		
	}
	

	public static void getCampaignValidation(JsonObject response) {

		response = response.get("data").getAsJsonObject();
		System.out.println(response);

		Assert.assertTrue(response.has("campaign_id"));
		Assert.assertTrue(response.has("campaign_name"));
		Assert.assertTrue(response.has("external_order_id"));
		Assert.assertTrue(response.has("trafficker_id"));
		Assert.assertTrue(response.has("purchase_order"));
		Assert.assertTrue(response.has("ad_property"));
		Assert.assertTrue(response.has("salesperson_id"));
		Assert.assertTrue(response.has("platform"));
		Assert.assertTrue(response.has("status"));
		Assert.assertTrue(response.has("notes"));
		Assert.assertTrue(response.has("advertiser_id"));
		// Assert.assertTrue(response.has("totalBudget"));
	}

	public static void updateCampaignValidation(JsonObject response) {

		response = response.get("data").getAsJsonObject();
		System.out.println(response);

		Assert.assertTrue(response.has("campaign_id"));
		Assert.assertTrue(response.has("campaign_name"));
		Assert.assertTrue(response.has("external_order_id"));
		Assert.assertTrue(response.has("trafficker_id"));
		Assert.assertTrue(response.has("purchase_order"));
		Assert.assertTrue(response.has("ad_property"));
		Assert.assertTrue(response.has("salesperson_id"));
		Assert.assertTrue(response.has("platform"));
		Assert.assertTrue(response.has("status"));
		Assert.assertTrue(response.has("notes"));
		Assert.assertTrue(response.has("advertiser_id"));
		// Assert.assertTrue(response.has("totalBudget"));
	}

	public static void createCreativeValidation(JsonObject response) {

		System.out.println(response);
		Assert.assertTrue(response.has("status"));
		Assert.assertTrue(response.has("totalResults"));
		response = response.get("data").getAsJsonObject();
		Assert.assertTrue(response.has("creative_id"));
	}
	

	public static void creativeCreateNegativeTestValidation(JsonObject response, String model_type, boolean native_banner, boolean static_banner, boolean placement) {
		
		
		System.out.println("debug 1");
		
		
		
		if (model_type == "bns" && native_banner==true && static_banner==false ) {
			
			System.out.println("debug 2");

			System.out.println("true" +response);
			Assert.assertTrue(response.has("status"));
			String status = response.get("status").getAsString();
			String message = response.get("message").getAsString();
			Assert.assertTrue(message.matches("Banner Native is not allowed in this case."), "Error: For modelType==BNS &&native_banner==true && static_banner==false, satisfies the condition ");
			Assert.assertTrue(status.matches("failure"));
			System.out.println("bns + native banner true");		
		}
		
		if (model_type == "bns" && native_banner==false && static_banner==false ) {
			System.out.println("debug 3");

			System.out.println("true" +response);
			Assert.assertTrue(response.has("status"));
			String status = response.get("status").getAsString();
			String message = response.get("message").getAsString();
			Assert.assertTrue(message.matches("Banner Native is not allowed in this case."));
			Assert.assertTrue(status.matches("failure"));
			System.out.println("native mkt + native banner true");	
			
			
		}
		
	if (model_type == "native_mkt" && native_banner==false && static_banner==false  && placement==false) {
		System.out.println("debug 4");

			System.out.println("true" +response);
			Assert.assertTrue(response.has("status"));
			String status = response.get("status").getAsString();
			Assert.assertTrue(status.matches("success"));
			System.out.println("native mkt + native banner false");		
		}
			
		
		if (model_type == "bns" && native_banner==true && static_banner==false  && placement==false) {
			
			System.out.println("true" +response);
			Assert.assertTrue(response.has("status"));
			String status = response.get("status").getAsString();
			Assert.assertTrue(status.matches("success"));
			System.out.println("bns mkt + native banner true");		
		}

		
		
		
	if (model_type == "radio" && native_banner==false && static_banner==false && placement==true ) {
			
		System.out.println("true" +response);
		Assert.assertTrue(response.has("status"));
		String status = response.get("status").getAsString();
		Assert.assertTrue(status.matches("success"));
	}
	
	}

	public static void getCreativeValidation(JsonObject response) {

		response = response.get("data").getAsJsonObject();
		System.out.println(response);
		Assert.assertTrue(response.has("creative_id"));
		Assert.assertTrue(response.has("creative_name"));
		Assert.assertTrue(response.has("status"));
		Assert.assertTrue(response.has("audio"));
		Assert.assertTrue(response.has("banner_static"));

	}

	public static void adsetByadIdValidation(JsonObject response) {
		Assert.assertTrue(response.has("status"));
		Assert.assertTrue(response.has("totalResults"));
		if (response.has("data")) {
			JsonObject data = response.get("data").getAsJsonObject();
			Assert.assertTrue(data.has("ad_set_ids"));
		}
	}

	public static void adSetCreateValidation(JsonObject response){
		if(response.has("data")){
			JsonObject data = response.get("data").getAsJsonObject();
			Assert.assertTrue(data.has("ad_set_id"));
		}
		Assert.assertTrue(response.has("status"));
		Assert.assertTrue(response.has("totalResults"));
	}

	public static void adSetGetValidation(JsonObject response, int detail){

		if(response.has("data")){
			JsonObject data = response.get("data").getAsJsonObject();
			Assert.assertTrue(data.has("campaign_id"));
			System.out.println("campaign_id");
			Assert.assertTrue(data.has("ad_set_id"));
			System.out.println("ad_set_id");
			Assert.assertTrue(data.has("ad_set_name"));
			System.out.println("ad_set_name");
			Assert.assertTrue(data.has("start_date"));
			System.out.println("start_date");
			Assert.assertTrue(data.has("end_date"));
			System.out.println("end_date");
			Assert.assertTrue(data.has("daily_budget"));
			System.out.println("daily_budget");
			Assert.assertTrue(data.has("segment_id"));
			System.out.println("segment_id");
			Assert.assertTrue(data.has("cost_per_unit"));
			System.out.println("cost_per_unit");
			Assert.assertTrue(data.has("platform"));
			System.out.println("platform");

			Assert.assertTrue(data.has("geo_country"));
			System.out.println("geo_country");
			Assert.assertTrue(data.has("geo_state"));
			System.out.println("geo_state");
			Assert.assertTrue(data.has("geo_city"));
			System.out.println("geo_city");


			Assert.assertTrue(data.has("external_id"));
			System.out.println("external_id");
			Assert.assertTrue(data.has("comments"));
			System.out.println("comments");
			Assert.assertTrue(data.has("status"));
			System.out.println("status");
//			Assert.assertTrue(data.has("priority"));
//			System.out.println("priority");
			Assert.assertTrue(data.has("last_transaction_details"));
			System.out.println("last_transaction_details");
			JsonObject lastTransDetails = data.get("last_transaction_details").getAsJsonObject();
			Assert.assertTrue(lastTransDetails.has("status"));
			System.out.println("status");
			Assert.assertTrue(lastTransDetails.has("details"));
			System.out.println("details");
			Assert.assertTrue(lastTransDetails.has("type"));
			System.out.println("type");
			Assert.assertTrue(lastTransDetails.has("ts"));
			System.out.println("ts");
			Assert.assertTrue(data.has("creatives"));
			System.out.println("creatives");

		}
	}

	public static void adIdSegmentValidation(JsonObject response) {
		Assert.assertTrue(response.has("status"));
		Assert.assertTrue(response.has("totalResults"));
		if (response.has("data")) {
			JsonObject data = response.get("data").getAsJsonObject();
			Assert.assertTrue(data.has("segment_ids"));
		}
	}

	public static void updateCreativeValidation(JsonObject response) {
		response = response.get("data").getAsJsonObject();
		System.out.println(response);
		Assert.assertTrue(response.has("creative_id"));
		Assert.assertTrue(response.has("creative_name"));
		// Assert.assertTrue(response.has("external_creative_id"));
		Assert.assertTrue(response.has("status"));
		Assert.assertTrue(response.has("audio"));
		Assert.assertTrue(response.has("banner_static"));
	}

	public static void deleteSegmentValidation(JsonObject response) {
		Assert.assertTrue(response.has("status"));
		Assert.assertTrue(response.has("totalResults"));
		Assert.assertTrue(response.has("data"));
	}

	public static void segmentValidation(JsonObject response) {
		System.out.println("Inside segmentValidation");
		System.out.println(response);
		Assert.assertTrue(response.has("status"));
		System.out.println("Status done");
		Assert.assertTrue(response.has("totalResults"));
		System.out.println("totalResults done");
		JsonObject data = response.get("data").getAsJsonObject();
		Assert.assertTrue(data.has("segment_id"));
		System.out.println("segment_id done");
		Assert.assertTrue(data.has("name"));
		System.out.println("name done");
		Assert.assertTrue(data.has("description"));
		System.out.println("description done");
		Assert.assertTrue(data.has("pending_jobs"));
		System.out.println("ingestion_status done");
		Assert.assertTrue(data.has("completed_jobs"));
		System.out.println("deleted done");
		Assert.assertTrue(data.has("warning"));
		System.out.println("warning done");
		Assert.assertTrue(data.has("count"));
		System.out.println("count done");
		Assert.assertTrue(data.has("deleted"));
		System.out.println("deleted done");

	}

}

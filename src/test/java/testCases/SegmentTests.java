/**
 * 
 */
package testCases;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import resources.GenerateToken;
import resources.Validator;

/**
 * @author aswin
 *
 */
public class SegmentTests {

	String server;
	

	@Test
	@Parameters ({"server"})
	public void segmentTests(String server) {
			this.server=server;
		 String segId = this.createSegment();
		 System.out.println("segment Id: " + segId);
		 this.getSegment(segId);
		 this.updateSegment(segId);
		 this.deleteSegment(segId);
		 segmentGetByAdID("4583921479");
		 getAdsetsBySegId(segId);

		// negativeTests();
		// negativeCreateCase();
		// negativeGet();
//		negativeDelete();
	}

	/*
	 * Create Segment test
	 */
	public String createSegment() {
		try {
			System.out.println("Inside create Segment...");
			System.out.println("Generating access token....");
			GenerateToken tokn = GenerateToken.getInstance();
				String accessToken = tokn.generateToken();
			System.out.println("Access token: " + accessToken);
			HttpClient client = HttpClientBuilder.create().build();
			String[] adids = { "\"DFFD3E5E-F01D-4BFA-ACB1-3509205020F0\"", "\"4583921479\"" };
			StringBuilder url = new StringBuilder(
					server+"/segment/create?access_token=" + accessToken+"&env=qa");
			System.out.println("url: " + url);

			HttpPost post = new HttpPost(url.toString());

			Random random = new Random();
			int val = random.nextInt(500000000);

			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new BasicNameValuePair("segment_name", String.valueOf(val)));
			params.add(new BasicNameValuePair("ad_ids", java.util.Arrays.toString(adids)));
			params.add(new BasicNameValuePair("type"	, "insert"));
			
			post.setEntity(new UrlEncodedFormEntity(params));
			System.out.println("printing the url " +post.toString());
			HttpResponse resp = client.execute(post);
			System.out.println("----Segment Create-----");
			
			String respString = EntityUtils.toString(resp.getEntity());
			System.out.println("Response: " + respString);
			JsonObject rJson = new Gson().fromJson(respString, JsonObject.class);
			
			System.out.println("Create Segment validation starts here...2..");
			Validator.segmentValidation(rJson);
			System.out.println("Create Segment validation ends here.....");

			if (rJson.has("data")) {
				JsonObject data = rJson.get("data").getAsJsonObject();
				if (data.has("segment_id")) {
					String segId = data.get("segment_id").getAsString();
					return segId;
				}
			}

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String getSegment(String segId) {
		try {
			String accessToken = GenerateToken.getInstance().generateToken();
			System.out.println("-------Get Segment------");
			HttpClient client = HttpClientBuilder.create().build();
			String url = server+"/segment/get?access_token=" + accessToken + "&segment_id="
					+ segId+"&env=qa";

			System.out.println("URL:" + url);
			HttpGet get = new HttpGet(url);
			HttpResponse resp = client.execute(get);
			String respStr = EntityUtils.toString(resp.getEntity());
			System.out.println(respStr);
			
			JsonObject segObj = new Gson().fromJson(respStr, JsonObject.class);
			System.out.println("Get Segment validation starts here.....");
			Validator.segmentValidation(segObj);
			System.out.println("Get Segment validation ends here.....");
			
			return respStr;
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void updateSegment(String segId) {
		try {
			String accessToken = GenerateToken.getInstance().generateToken();
			Random random = new Random();
			int num = random.nextInt(1000000000);
			String val = String.valueOf(num);

			System.out.println("> Getting the original segment");
			String b4Upd = this.getSegment(segId);
			JsonObject b4Json = new Gson().fromJson(b4Upd, JsonObject.class);

			System.out.println("> Updating with new values");
			HttpClient client = HttpClientBuilder.create().build();
			String[] adids = { "\"DFFD3E5E-F01D-4BFA-ACB1-3509205020F0\"" };

			StringBuilder url = new StringBuilder(server+"/segment/update?access_token="
					+ accessToken + "&segment_id=" + segId + "&segment_name=" + val
					+ "&ad_ids=%5b%22DFFD3E5E-F01D-4BFA-ACB1-3509205020F0%22%5d"+"&env=qa");

			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("segment_id", segId));
			params.add(new BasicNameValuePair("segment_name", val));
			params.add(new BasicNameValuePair("type", "delete"));
			params.add(new BasicNameValuePair("ad_ids", java.util.Arrays.toString(adids)));

			HttpPost post = new HttpPost(url.toString());
			post.setEntity(new UrlEncodedFormEntity(params));
			System.out.println("printing the url2 " +post.toString());
			HttpResponse resp = client.execute(post);

			String respStr = EntityUtils.toString(resp.getEntity());
			System.out.println("> Updated Response3");
			System.out.println(respStr);

			// JsonObject afterJson = new Gson().fromJson(respStr,
			// JsonObject.class);
			//
			// String origName = null;
			//
			// if (b4Json.has("data")) {
			// if (b4Json.get("data").getAsJsonObject().has("name")) {
			// origName =
			// b4Json.get("data").getAsJsonObject().get("name").getAsString();
			// }
			// }
			// String updated = null;
			// if (afterJson.has("data")) {
			// if (afterJson.get("data").getAsJsonObject().has("name")) {
			// updated =
			// afterJson.get("data").getAsJsonObject().get("name").getAsString();
			// }
			// }
			//
			// Assert.assertFalse(origName.equals(updated));
			JsonObject segObj = new Gson().fromJson(respStr, JsonObject.class);
			System.out.println("Update Segment validation starts here.....");
			Validator.segmentValidation(segObj);
			System.out.println("Update Segment validation ends here.....");

			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void deleteSegment(String segId) {
		System.out.println("------Delete Segment------");
		try {
			String accessToken = GenerateToken.getInstance().generateToken();
			HttpClient client = HttpClientBuilder.create().build();
			String url = server+"/segment/delete?access_token=" + accessToken + "&segment_id="
					+ segId+"&env=qa" ;
			HttpPut put = new HttpPut(url);

			HttpResponse res = client.execute(put);
			String resStr = EntityUtils.toString(res.getEntity());
			System.out.println("> Delete Response");
			System.out.println(resStr);

			JsonObject json = new Gson().fromJson(resStr, JsonObject.class);
			
			System.out.println("Delete Segment validation starts here.....");
			Validator.deleteSegmentValidation(json);
			System.out.println("Delete Segment validation ends here.....");

			if (json.has("data")) {
				System.out.println("printing json " +json);
				JsonObject data = json.get("data").getAsJsonObject();
				if (data.has("deleted")) {
					System.out.println("bool " +data.get("deleted").getAsBoolean());
					//Assert.assertTrue(data.get("deleted").getAsBoolean());
				}
			}

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void segmentGetByAdID(String adId) {
		String accessToken = GenerateToken.generateToken();
		try {
			HttpClient client = HttpClientBuilder.create().build();
			String url = server+"/segment/getSegmentByAdId?access_token=" + accessToken
					+ "&ad_id="+adId+"&env=qa";

			HttpGet get = new HttpGet(url);

			HttpResponse res = client.execute(get);
			String resStr = EntityUtils.toString(res.getEntity());
			System.out.println("Get Segment by AdID: " + resStr);
			
			JsonObject json = new Gson().fromJson(resStr, JsonObject.class);
			System.out.println("Get Segment By Ad ID validation starts here.....");
			Validator.adIdSegmentValidation(json);
			System.out.println("Get Segment By Ad ID validation ends here.....");

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void getAdsetsBySegId(String segId) {
		System.out.println("Inside getAdsetsBySegId");
		try {
			String accessToken = GenerateToken.generateToken();
			HttpClient client = HttpClientBuilder.create().build();
			String url = server+"/segment/getAdSetsBySegmentId?access_token=" + accessToken
					+ "&segment_id=" + segId +"&env=qa";
			System.out.println("segId: " + segId);
			System.out.println("URL: " + url);
			HttpGet get = new HttpGet(url);

			HttpResponse res = client.execute(get);
			String response = EntityUtils.toString(res.getEntity());
			System.out.println("Get Adset By SegId --- Response");
			System.out.println(response);
			
			JsonObject json = new Gson().fromJson(response, JsonObject.class);
			System.out.println("Get Segment By Ad ID validation starts here.....");
			Validator.adsetByadIdValidation(json);
			System.out.println("Get Segment By Ad ID validation ends here.....");

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void negativeTests() {
		
		String apiToken = GenerateToken.generateToken("SaavnAPITestCLient", "SaavnAPITestPass");
		String testToken = GenerateToken.generateToken("testclient", "testpass");
		String accessToken = GenerateToken.generateToken();
		accessToken = testToken;
		String segId = createSegment();
		System.out.println("SegId: " + segId);
		System.out.println("Segment created using testToken");

		System.out.println("-------------------");
		System.out.println("Segment accessed using apiToken");
		accessToken = apiToken;
		getSegment(segId);

	}

	public void negativeCreateCase() {
		try {
			String accessToken = GenerateToken.generateToken();
			// System.out.println("Inside create Segment");
			HttpClient client = HttpClientBuilder.create().build();
			String[] adids = { "\"uchiha-madara\"", "\"uchiha-sasuke\"" };
			StringBuilder url = new StringBuilder(
					server+"/segment/create?access_token=" + accessToken);

			HttpPost post = new HttpPost(url.toString());

			Random random = new Random();
			int val = random.nextInt(500000000);

			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new BasicNameValuePair("segment_name", String.valueOf(val)));
			params.add(new BasicNameValuePair("ad_ids", java.util.Arrays.toString(adids)));
			// System.out.println("PARAMS ARRAY LIST");
			// System.out.println(params.toString());
			post.setEntity(new UrlEncodedFormEntity(params));

			HttpResponse resp = client.execute(post);
			System.out.println("----Segment Create-----");
			// System.out.println(EntityUtils.toString(resp.getEntity()));
			String respString = EntityUtils.toString(resp.getEntity());
			System.out.println("Response: " + respString);
			JsonObject rJson = new Gson().fromJson(respString, JsonObject.class);

			if (rJson.has("data")) {
				JsonObject data = rJson.get("data").getAsJsonObject();
				if (data.has("segment_id")) {
					String segId = data.get("segment_id").getAsString();
					System.out.println("seg ID: " + segId);
				}
			}

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void negativeGet() {
		try {
			String accessToken = GenerateToken.generateToken();
			System.out.println("-------Get Segment------");
			HttpClient client = HttpClientBuilder.create().build();
			String url = server+"/segment/get?access_token=" + accessToken + "&segment_id=)(*&";
			HttpGet get = new HttpGet(url);
			HttpResponse resp = client.execute(get);
			String respStr = EntityUtils.toString(resp.getEntity());
			System.out.println(respStr);

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void negativeDelete() {
		String segId = createSegment();
		deleteSegment(segId);
		deleteSegment(segId);
		getSegment(segId);

		deleteSegment("asdasdasdasd");
	}

}

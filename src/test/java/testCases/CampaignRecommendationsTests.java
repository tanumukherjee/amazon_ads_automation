package testCases;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Properties;

import org.testng.AssertJUnit;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import resources.GenerateToken;
import resources.GetConnection;
import resources.RecommendationsValidator;
import resources.Validator;

public class CampaignRecommendationsTests {

	Gson gson = new Gson();
	Properties prop;
	String server;

	
	
	
	@Test 
	@Parameters ({"server"})
	public void tests(String server) throws Exception {
		this.server=server;
	//	GenerateToken tokn = GenerateToken.getInstance();
		String accessToken = GenerateToken.generateToken();
		prop = new Properties();
		FileInputStream fs = new FileInputStream(
				"src/main/java/resources/CampaignProperties.properties");
		prop.load(fs);
		String campaignName = URLEncoder.encode(prop.getProperty("campaignName"), "UTF-8");
		String frequencyCap = URLEncoder.encode(prop.getProperty("frequency_cap"), "UTF-8");
		String campaignPriority = URLEncoder.encode(prop.getProperty("campaign_priority"), "UTF-8");
		String platform = URLEncoder.encode(prop.getProperty("platform"), "UTF-8");
		String externalCampaignId = resources.Utilities.generateNumber();
		String purchaseOrderId = URLEncoder.encode(prop.getProperty("purchase_order_id"), "UTF-8");

		String notes = URLEncoder.encode(prop.getProperty("notes"), "UTF-8");

		/*
		 * Calling the createCampaign mathod for getting the /campaign ID
		 */
		String campaignId = createCampaign(  accessToken,  campaignName,  platform,
				 purchaseOrderId,  notes,  campaignPriority,  frequencyCap,  externalCampaignId);
		System.out.println("/campaign ID is " + campaignId);
		/*
		 * Getting the /campaign details by passing the /campaign ID to getCampaignId
		 * method
		 */
		JsonObject olderCampaignResponse = getCampaign(accessToken, campaignId);

		/*
		 * Updating the /campaign
		 */
		JsonObject updatedCampaignResponse = updateCampaign(accessToken, campaignId);

		/*
		 * Checking if the repsonse differs for both the order ids
		 */
		verifyUpdateCampaign(olderCampaignResponse, updatedCampaignResponse);
		/*
		 * delete /campaign
		 */
		deleteCampaign(campaignId, accessToken);
		/*
		 * Verify delete /campaign, after deleting, checkint the response by calling the
		 * getCampaign method
		 */

		verifyDeleteCampaign(accessToken, campaignId);
//		
//		/*
//		 * Verify delete /campaign, after deleting, checkint the response by calling the
//		 * getCampaign method
//		 */
//		String [] statusList  = {"unarchive", "pause", "resume"};
//	
//		for(int i = 0; i<statusList.length;i++) {
//			JsonObject updatedCampaignStatusResponse = updateCampaignStatus(accessToken, campaignId, statusList[i]); 
//			System.out.println("order resp : " +olderCampaignResponse);
//			System.out.println("upd resp : " +updatedCampaignStatusResponse);
//			//verifyUpdateStatus(olderCampaignResponse, updatedCampaignStatusResponse);
//		}
//		
		 

		
		
	}
	

	public String createCampaign( String accessToken, String campaignName, String platform,
			String purchaseOrderId, String notes, String campaignPriority, String frequencyCap, String externalCampaignId) throws Exception {
		

		System.out.println("-------Create campaign------");

		
		String url = this.server+"/campaign/create?&access_token=" + accessToken + "&campaign_name="
				+ campaignName + "&purchase_order_id=" + purchaseOrderId + "&notes=" + notes + "&env=qa"+ "&frequency_cap=" +frequencyCap+"&campaign_priority="+campaignPriority+"&external_campaign_id="+externalCampaignId;
		
		
		System.out.println("URL is " +url);
		URL con= new URL(url);
		HttpURLConnection httpCon =  (HttpURLConnection) con.openConnection();
		httpCon.setRequestMethod("POST");
		BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
		String inputLine;
		String resp="";
		while((inputLine=in.readLine())!=null) {
			resp=resp.concat(inputLine);
		}

		JsonObject respObject = gson.fromJson(resp, JsonObject.class);
		// Starting validation here
		System.out.println("Starting create /campaign validation");
		RecommendationsValidator.createCampaignValidation(respObject);
		String campaignId = respObject.get("data").getAsJsonObject().get("campaign_id").getAsString();
		System.out.println("/campaign is created, ID is " + campaignId);
		return campaignId;

	}
	
	
	
		public JsonObject getCampaign( String accessToken, String campaignId) throws Exception {
		
		System.out.println("-------Get campaign------ \n");

		
		String url = this.server+"/campaign/get&access_token=" + accessToken + "&campaign_id="
				+ campaignId + "&env=qa";
		String resp = GetConnection.getConnectionDetails(url);
		JsonObject respObj = gson.fromJson(resp, JsonObject.class).getAsJsonObject();
		System.out.println("Starting get campaign validation");
		RecommendationsValidator.getCampaignPARValidation(respObj);
		JsonObject olderCampaignResponse = gson.fromJson(resp, JsonObject.class).get("data").getAsJsonObject();
		System.out.println("Older campaign response is " + olderCampaignResponse);
		return olderCampaignResponse;

	}
		
		
		
		public JsonObject updateCampaign(String accessToken, String campaignId) throws Exception {

			System.out.println("-------Update campaign------ \n");
			
			/*
			 * Updating the /campaign by changing the values of all the fields
			 */
			
			String UcampaignName = URLEncoder.encode(prop.getProperty("UcampaignName"), "UTF-8");
			String frequencyCap = URLEncoder.encode(prop.getProperty("Ufrequency_cap"), "UTF-8");
			String campaignPriority = URLEncoder.encode(prop.getProperty("Ucampaign_priority"), "UTF-8");
			String platform = URLEncoder.encode(prop.getProperty("Uplatform"), "UTF-8");
			String externalCampaignId = resources.Utilities.generateNumber();
			String purchaseOrderId = URLEncoder.encode(prop.getProperty("Upurchase_order_id"), "UTF-8");
			String status = URLEncoder.encode(prop.getProperty("Ustatus"), "UTF-8");
			String notes = URLEncoder.encode(prop.getProperty("Unotes"), "UTF-8");
			

			String url = this.server+"/campaign/update?access_token=" + accessToken + "&campaign_id="
					+ campaignId + "&campaign_name=" + UcampaignName + "&status=" + status +
					 "&notes=" + notes +"&campaign_priority="+campaignPriority +"&frequency_cap="+frequencyCap
					+ "&env=qa"+ "&purchase_order_id="+purchaseOrderId;
			System.out.println("Update campaign url "+url);
			String resp = GetConnection.getConnectionDetailsPostRequest(url);
			JsonObject respObj = gson.fromJson(resp, JsonObject.class);
			System.out.println("update3 json " +resp);
			// Validating the keys
			RecommendationsValidator.updateCampaignPARValidation(respObj);
			System.out.println(resp);
			JsonObject updatedCampaignResponse = gson.fromJson(resp, JsonObject.class).get("data").getAsJsonObject();
			System.out.println("campaign is updated!");
			return updatedCampaignResponse;
		}
		
			public void verifyUpdateCampaign(JsonObject olderCampaignResponse, JsonObject updatedCampaignResponse)
				throws JsonParseException, IOException {
			
			System.out.println("-------Verify Update campaign------ \n");

			System.out.println("Older campaign details " + olderCampaignResponse);
			System.out.println("Updated campaing details " + updatedCampaignResponse);

			//AssertJUnit.assertFalse(olderCampaignResponse.get("campaign_name").equals(updatedCampaignResponse.get("campaign_name")));
			AssertJUnit.assertFalse(olderCampaignResponse.get("frequency_cap").equals(updatedCampaignResponse.get("frequency_cap")));
			AssertJUnit.assertFalse(olderCampaignResponse.get("campaign_priority").equals(updatedCampaignResponse.get("campaign_priority")));
			AssertJUnit.assertFalse(olderCampaignResponse.get("purchase_order_id").equals(updatedCampaignResponse.get("purchase_order_id")));
			AssertJUnit.assertFalse(olderCampaignResponse.get("notes").equals(updatedCampaignResponse.get("notes")));
            
			System.out.println("Verified : values are different");

		}
			
			public void deleteCampaign(String campaignId, String accessToken) throws Exception {
				
				System.out.println("-------Delete campaign------ \n");
				
				String url = this.server+"/campaign/delete?campaign_id=" + campaignId + "&access_token="
						+ accessToken + "&env=qa";
				String resp = GetConnection.getConnectionDetailsPutRequest(url);
				System.out.println("SoftDelete: " + resp);
				// JsonObject respObj = gson.fromJson(resp,
				// JsonObject.class).get("data").getAsJsonObject();
				String status = gson.fromJson(resp, JsonObject.class).getAsJsonObject().get("status").getAsString();
				System.out.println(status);
				AssertJUnit.assertTrue(status.equals("success"));
				System.out.println("campaign is deleted");
			}
			
			public void verifyDeleteCampaign(String accessToken, String campaignId) throws Exception {
				
				System.out.println("-------Verify Delete campaign------ \n");
				
				String url = this.server+"/campaign/get&access_token=" + accessToken + "&campaign_id="
						+ campaignId + "&env=qa";
				String resp = GetConnection.getConnectionDetails(url);
				System.out.println("Verify delete campaign: Deleted " + resp);
				String archiveStatus = gson.fromJson(resp, JsonObject.class).getAsJsonObject().get("status").getAsString();
				System.out.println("archive status is " +archiveStatus);
				AssertJUnit.assertTrue(archiveStatus.equals("success"));
//				String campaignResponse = gson.fromJson(resp, JsonObject.class).getAsJsonObject().get("status").getAsString();
//				AssertJUnit.assertTrue(campaignResponse.equals("success"));

			}
		
		
	
}

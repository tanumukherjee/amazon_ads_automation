package testCases;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import resources.CreativeCreateEntities;
import resources.EntityBuilder;
import resources.GenerateToken;
import resources.GetConnection;
import resources.Validator;

public final class CreativeTests {

	Gson gson = new Gson();
	
	String server;
	// protected String accessToken;

	@Test
	@Parameters({ "server" })
	public void creativeTests(String server) throws Exception {
		this.server = server;
		// Calling the access token method to generate access token
		// generateToken();
		GenerateToken tokn = GenerateToken.getInstance();
		String access_token = tokn.generateToken();
		// String access_token = "f57325948eccf98488e719b217aee5acf95a51e0";
		System.out.println("acc token = "+access_token);
		/*
		 * Creating creative tests by passing access token and other entities
		 */
		String creativeId = createCreative(access_token);
		System.out.println("creativeId  " +creativeId);
		// Creative create negative cases
//		
//		String [] modelType =  {"bns" , "native_mkt" ,"radio"};
//		Boolean[] nativeBanner  = {true, false};
//		
//		modelType , native_mkt , static banner 
//		bns , true, true;
//		
//		
//		ArrayList<String[] > list = new ArrayList<String[]>();
//
//		
//		list
//		
//		String[] model_type = {"bns" , "native_mkt" ,"radio"};
////		
//		for(int i=0; i<model_type.length ;i++) {
//			
//		creativeCreateNegativeCases(access_token, model_type[i],adSpot, audio, placement);
//		}
//		
//		list.add(model_type[i], element);
//		
//		String [][] arrStr = {"model_type", "native_banner"} 
//		boolean placement = false;
		
		System.out.println("Test case: BNS + Native Banner true + static banner false = Success \n");
		CreativeCreateEntities entity = new EntityBuilder().setModelType("bns").setNativeBanner(true).setStaticBanner(false).setPlacement(false).getEntities();
		creativeCreateNegativeCases(access_token, entity.getModelType(), entity.isNativeBanner(),entity.isStaticBanner(), entity.isPlacement());

		System.out.println("Test case: BNS + Native Banner false  + Static banner false= Failure \n");
		entity = new EntityBuilder().setModelType("bns").setNativeBanner(false).setStaticBanner(false).setPlacement(false).getEntities();
		creativeCreateNegativeCases(access_token, entity.getModelType(), entity.isNativeBanner(),entity.isStaticBanner(),entity.isPlacement());
		
		System.out.println("Test case: BNS + Native Banner False + Static Banner True + Placement True = Success \n");
		
		entity = new EntityBuilder().setModelType("bns").setNativeBanner(false).setStaticBanner(true).setPlacement(true).getEntities();
		creativeCreateNegativeCases(access_token, entity.getModelType(), entity.isNativeBanner(),entity.isStaticBanner(),entity.isPlacement());
		
		System.out.println("Test case: BNS + Native Banner False + Static Banner True + Placement False = Failure \n");
		
		entity = new EntityBuilder().setModelType("bns").setNativeBanner(false).setStaticBanner(true).setPlacement(false).getEntities();
		creativeCreateNegativeCases(access_token, entity.getModelType(), entity.isNativeBanner(),entity.isStaticBanner(),entity.isPlacement());
		

		System.out.println("Test case: Native_mkt + Native Banner True  + Static banner True= Success \n");
		entity = new EntityBuilder().setModelType("native_mkt").setNativeBanner(true).setStaticBanner(true).setPlacement(true).getEntities();
		creativeCreateNegativeCases(access_token, entity.getModelType(), entity.isNativeBanner(),entity.isStaticBanner(), entity.isPlacement());
		
		
		System.out.println("Test case: Native_mkt + Native Banner True  + Static banner False= Success \n");
		entity = new EntityBuilder().setModelType("native_mkt").setNativeBanner(true).setStaticBanner(false).setPlacement(false).getEntities();
		creativeCreateNegativeCases(access_token, entity.getModelType(), entity.isNativeBanner(),entity.isStaticBanner(), entity.isPlacement());
		
		System.out.println("Test case: Native_mkt + Native Banner false + Static banner False = Error \n");
		entity = new EntityBuilder().setModelType("native_mkt").setNativeBanner(false).setStaticBanner(false).setPlacement(false).getEntities();
		creativeCreateNegativeCases(access_token, entity.getModelType(), entity.isNativeBanner(),entity.isStaticBanner(), entity.isPlacement());
		
		System.out.println("Test case: Native_mkt + Native Banner false + Static banner True + Placement True = Error \n");
		entity = new EntityBuilder().setModelType("native_mkt").setNativeBanner(false).setStaticBanner(false).setPlacement(true).getEntities();
		creativeCreateNegativeCases(access_token, entity.getModelType(), entity.isNativeBanner(),entity.isStaticBanner(), entity.isPlacement());
		
		System.out.println("Test case: Native_mkt + Native Banner false + Static banner True + Placement False  = Error \n");
		entity = new EntityBuilder().setModelType("native_mkt").setNativeBanner(false).setStaticBanner(false).setPlacement(false).getEntities();
		creativeCreateNegativeCases(access_token, entity.getModelType(), entity.isNativeBanner(),entity.isStaticBanner(), entity.isPlacement());
		
		
		
		System.out.println("XXXXXXXXXXXXX=========RADIO test cases===============XXXXXXXXXXXXXXXXX \n");
		
		
		System.out.println("Test case: radio + Native Banner false + Static banner True + Placement True  = Success \n");
		entity = new EntityBuilder().setModelType("radio").setNativeBanner(false).setStaticBanner(true).setPlacement(true).getEntities();
		creativeCreateNegativeCases(access_token, entity.getModelType(), entity.isNativeBanner(),entity.isStaticBanner(), entity.isPlacement());
		
		


		
	//	}
		// Calling the creative get method
		JsonObject olderCreativeResponse = getCreative(access_token, creativeId);
		// Calling the update method
		JsonObject updatedCreativeResponse = updateCreative(access_token, creativeId);
		// Verifying the update call
		verifyUpdateCreative(olderCreativeResponse, updatedCreativeResponse);
		// Deleting creative call
		deleteCreative(access_token, creativeId);
		// Verifying if it is deleted and archived properly
		verifyDelete(access_token, creativeId);

	}

	public String createCreative(String accessToken)
			throws NumberFormatException, ClientProtocolException, IOException {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost httpPost;
		ArrayList<NameValuePair> postParameters;
		httpPost = new HttpPost(server + "/creative/create?access_token=" + accessToken + "&env=qa");
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("audio",
				"[{\"type\":\"audio\",\"media_id\":\"tc_ops_g_abm01\",\"bitrate\":\"44100\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid_audio.mp3\",\"duration\":\"00:00:17:000\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-audio-start-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"}]"));
		postParameters.add(new BasicNameValuePair("banner_static",
				"[{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab01\",\"placement\":\"audioCompanion\",\"width\":\"300\",\"height\":\"250\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-300x250.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-audio-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-audio-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"},{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab02\",\"placement\":\"spotlight\",\"width\":\"320\",\"height\":\"86\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x86.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-spotlight-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-spotlight-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"},{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab03\",\"placement\":\"banner\",\"width\":\"320\",\"height\":\"50\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x50.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-banner-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-banner-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"},{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab04\",\"placement\":\"interstitial\",\"width\":\"320\",\"height\":\"480\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x480.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-idle-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-idle-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"}]"));
		//postParameters.add(new BasicNameValuePair(name, value))
		postParameters.add(new BasicNameValuePair("creative_name", "renault_test"));
		postParameters.add(new BasicNameValuePair("is_display_native", "true"));
		postParameters.add(new BasicNameValuePair("model_type", "bns"));
		String external_creative_id = java.util.UUID.randomUUID().toString();
		System.out.println("external_creative_id: " + external_creative_id);
		postParameters.add(new BasicNameValuePair("external_creative_id", external_creative_id));
		httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
		System.out.println("http post: " + httpPost.toString());
		HttpResponse objResponse = httpClient.execute(httpPost);
		// HttpResponse objResponse = httpClient.execute(httpPost);
		System.out.println("uuuuuurrrrlllll is : " + objResponse);
		HttpEntity entity = objResponse.getEntity();
		String responseString = EntityUtils.toString(entity, "UTF-8");
		// System.out.println("uuuuuurrrrlllll is " +responseString);
		System.out.println("resp string: " + responseString);
		JsonObject resp = gson.fromJson(responseString, JsonObject.class);
		System.out.println("starting validations for create creative");
		Validator.createCreativeValidation(resp);
		// System.out.println(resp);
		String creativeId = resp.get("data").getAsJsonObject().get("creative_id").getAsString();
		return creativeId;
	}

	public void creativeCreateNegativeCases(String accessToken, String model_type, boolean native_banner, boolean static_banner ,boolean placement) throws ClientProtocolException, IOException {

		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost httpPost;
		ArrayList<NameValuePair> postParameters;
		httpPost = new HttpPost(server + "/creative/create?access_token=" + accessToken + "&env=qa");
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("audio",
				"[{\"type\":\"audio\",\"media_id\":\"tc_ops_g_abm01\",\"bitrate\":\"44100\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid_audio.mp3\",\"duration\":\"00:00:17:000\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-audio-start-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"}]"));
		postParameters.add(new BasicNameValuePair("creative_name", "renault_test"));
		postParameters.add(new BasicNameValuePair("is_display_native", "true"));

		if (model_type == "bns" && native_banner==true && static_banner==false ) {

			postParameters.add(new BasicNameValuePair("model_type", model_type));
			postParameters.add(new BasicNameValuePair("banner_native","[{\"type\":\"banner_native\",\"media_id\":\"vivek_native\",\"headline\":\"VivekisAwesome\",\"body\":\"Omgheisthebest!\",\"image\":\"http://s.saavn.com/ads/2017/amz_prod_img1.jpg\",\"call_to_action\":\"LearnMore\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"logo\":\"http://s.saavn.com/ads/2017/amz_logo_img.jpg\",\"fallback_click_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RZXX\"}]"));
			String external_creative_id = java.util.UUID.randomUUID().toString();
			System.out.println("external_creative_id: " + external_creative_id);
			postParameters.add(new BasicNameValuePair("external_creative_id", external_creative_id));
			httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
			System.out.println("http post: " + httpPost.toString());
			HttpResponse objResponse = httpClient.execute(httpPost);
			System.out.println("URLLLL is : " + objResponse);
			HttpEntity entity = objResponse.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			System.out.println("resp string: " + responseString);
			JsonObject resp = gson.fromJson(responseString, JsonObject.class);
			System.out.println("final resp" +resp);
			Validator.creativeCreateNegativeTestValidation(resp, model_type,  native_banner,  static_banner,   placement);		
		}
		

		if (model_type == "bns" && native_banner==false && static_banner==false ) {

			postParameters.add(new BasicNameValuePair("model_type", model_type));
			String external_creative_id = java.util.UUID.randomUUID().toString();
			System.out.println("external_creative_id: " + external_creative_id);
			postParameters.add(new BasicNameValuePair("external_creative_id", external_creative_id));
			httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
			System.out.println("http post: " + httpPost.toString());
			HttpResponse objResponse = httpClient.execute(httpPost);
			System.out.println("URLLLL is : " + objResponse);
			HttpEntity entity = objResponse.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			System.out.println("resp string: " + responseString);
			JsonObject resp = gson.fromJson(responseString, JsonObject.class);
			System.out.println("final resp" +resp);
			Validator.creativeCreateNegativeTestValidation(resp, model_type,  native_banner,  static_banner,   placement);		
		}
		
		
		if (model_type == "bns" && native_banner==false && static_banner==true  && placement==true) {

			postParameters.add(new BasicNameValuePair("model_type", model_type));		
			postParameters.add(new BasicNameValuePair("banner_static",
					"[{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab01\",\"placement\":\"audioCompanion\",\"width\":\"300\",\"height\":\"250\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-300x250.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-audio-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-audio-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"},{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab02\",\"placement\":\"spotlight\",\"width\":\"320\",\"height\":\"86\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x86.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-spotlight-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-spotlight-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"},{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab03\",\"placement\":\"banner\",\"width\":\"320\",\"height\":\"50\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x50.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-banner-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-banner-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"},{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab04\",\"placement\":\"interstitial\",\"width\":\"320\",\"height\":\"480\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x480.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-idle-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-idle-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"}]"));
			String external_creative_id = java.util.UUID.randomUUID().toString();
			System.out.println("external_creative_id: " + external_creative_id);
			postParameters.add(new BasicNameValuePair("external_creative_id", external_creative_id));
			httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
			System.out.println("http post: " + httpPost.toString());
			HttpResponse objResponse = httpClient.execute(httpPost);
			System.out.println("URLLLL is : " + objResponse);
			HttpEntity entity = objResponse.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			// System.out.println("uuuuuurrrrlllll is " +responseString);
			System.out.println("resp string: " + responseString);
			JsonObject resp = gson.fromJson(responseString, JsonObject.class);
			System.out.println("final resp" +resp);
			Validator.creativeCreateNegativeTestValidation(resp, model_type,  native_banner,  static_banner,   placement);		
		}
		
		if (model_type == "bns" && native_banner==true && static_banner==true  && placement==false) {

			postParameters.add(new BasicNameValuePair("model_type", model_type));	
			postParameters.add(new BasicNameValuePair("banner_native","[{\"type\":\"banner_native\",\"media_id\":\"vivek_native\",\"headline\":\"VivekisAwesome\",\"body\":\"Omgheisthebest!\",\"image\":\"http://s.saavn.com/ads/2017/amz_prod_img1.jpg\",\"call_to_action\":\"LearnMore\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"logo\":\"http://s.saavn.com/ads/2017/amz_logo_img.jpg\",\"fallback_click_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RZXX\"}]"));
			postParameters.add(new BasicNameValuePair("banner_static",
					"[{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab03\",\"placement\":\"banner\",\"width\":\"320\",\"height\":\"50\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x50.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"https://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"https://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-banner-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"https://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-banner-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"},{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab04\",\"placement\":\"interstitial\",\"width\":\"320\",\"height\":\"480\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x480.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"https://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"https://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-idle-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"https://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-idle-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"}]"));
			String external_creative_id = java.util.UUID.randomUUID().toString();
			System.out.println("external_creative_id: " + external_creative_id);
			postParameters.add(new BasicNameValuePair("external_creative_id", external_creative_id));
			httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
			System.out.println("http post: " + httpPost.toString());
			HttpResponse objResponse = httpClient.execute(httpPost);
			System.out.println("URLLLL is : " + objResponse);
			HttpEntity entity = objResponse.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			// System.out.println("uuuuuurrrrlllll is " +responseString);
			System.out.println("resp string: " + responseString);
			JsonObject resp = gson.fromJson(responseString, JsonObject.class);
			System.out.println("final resp" +resp);
			Validator.creativeCreateNegativeTestValidation(resp, model_type,  native_banner,  static_banner,   placement);		
		}
		
		
		
		
		else if (model_type == "native_mkt" && native_banner==false && static_banner==false && placement==false) {

			postParameters.add(new BasicNameValuePair("model_type", model_type));		
			String external_creative_id = java.util.UUID.randomUUID().toString();
			System.out.println("external_creative_id: " + external_creative_id);
			postParameters.add(new BasicNameValuePair("external_creative_id", external_creative_id));
			httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
			System.out.println("http post: " + httpPost.toString());
			HttpResponse objResponse = httpClient.execute(httpPost);
			System.out.println("URLLLL is : " + objResponse);
			HttpEntity entity = objResponse.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			System.out.println("resp string: " + responseString);
			JsonObject resp = gson.fromJson(responseString, JsonObject.class);
			System.out.println("resping man");
			Validator.creativeCreateNegativeTestValidation(resp,model_type,  native_banner,  static_banner,  placement);		
		}

		
		else if (model_type == "native_mkt" && native_banner==true && static_banner==false ) {

			postParameters.add(new BasicNameValuePair("model_type", model_type));		
			postParameters.add(new BasicNameValuePair("banner_native","[{\"type\":\"banner_native\",\"media_id\":\"vivek_native\",\"headline\":\"VivekisAwesome\",\"body\":\"Omgheisthebest!\",\"image\":\"http://s.saavn.com/ads/2017/amz_prod_img1.jpg\",\"call_to_action\":\"LearnMore\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"logo\":\"http://s.saavn.com/ads/2017/amz_logo_img.jpg\",\"fallback_click_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RZXX\"}]"));
			String external_creative_id = java.util.UUID.randomUUID().toString();
			System.out.println("external_creative_id: " + external_creative_id);
			postParameters.add(new BasicNameValuePair("external_creative_id", external_creative_id));
			httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
			System.out.println("http post: " + httpPost.toString());
			HttpResponse objResponse = httpClient.execute(httpPost);
			System.out.println("URLLLL is : " + objResponse);
			HttpEntity entity = objResponse.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			System.out.println("resp string: " + responseString);
			JsonObject resp = gson.fromJson(responseString, JsonObject.class);
			System.out.println("resping man");
			Validator.creativeCreateNegativeTestValidation(resp,model_type,  native_banner,  static_banner,   placement);		
		}

		if (model_type == "native_mkt") {
			postParameters.add(new BasicNameValuePair("model_type", model_type));
			postParameters.add(new BasicNameValuePair("banner_native", "[{\"type\":\"banner_native\",\"media_id\":\"vivek_native\",\"headline\":\"VivekisAwesome\",\"body\":\"Omgheisthebest!\",\"image\":\"http://s.saavn.com/ads/2017/amz_prod_img1.jpg\n" + 
					"\",\"call_to_action\":\"LearnMore\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"logo\":\"http://s.saavn.com/ads/2017/amz_logo_img.jpg\",\"fallback_clic\n" + 
					"k_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RZXX\"}] "));

		}

		if (model_type == "radio" && native_banner==false && static_banner==true && placement==true ) {
			postParameters.add(new BasicNameValuePair("model_type", model_type));
			postParameters.add(new BasicNameValuePair("banner_static",
					"[{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab01\",\"placement\":\"audioCompanion\",\"width\":\"300\",\"height\":\"250\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-300x250.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-audio-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-audio-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"},{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab02\",\"placement\":\"spotlight\",\"width\":\"320\",\"height\":\"86\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x86.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-spotlight-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-spotlight-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"},{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab03\",\"placement\":\"banner\",\"width\":\"320\",\"height\":\"50\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x50.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-banner-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-banner-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"},{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab04\",\"placement\":\"interstitial\",\"width\":\"320\",\"height\":\"480\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x480.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-idle-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-idle-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"}]"));			
			String external_creative_id = java.util.UUID.randomUUID().toString();
			System.out.println("external_creative_id: " + external_creative_id);
			postParameters.add(new BasicNameValuePair("external_creative_id", external_creative_id));
			httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
			System.out.println("http post: " + httpPost.toString());
			HttpResponse objResponse = httpClient.execute(httpPost);
			System.out.println("URLLLL is : " + objResponse);
			HttpEntity entity = objResponse.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			System.out.println("resp string: " + responseString);
			JsonObject resp = gson.fromJson(responseString, JsonObject.class);
			System.out.println("resping man");
			Validator.creativeCreateNegativeTestValidation(resp, model_type,  native_banner,  static_banner,   placement);	
			
		}
		postParameters.add(new BasicNameValuePair("model_type", "bns"));

	}

	public JsonObject getCreative(String accessToken, String creativeId) throws Exception {

		String url = server + "/api/v1/creative/get?access_token=" + accessToken + "&creative_id=" + creativeId
				+ "&env=qa";
		String resp = GetConnection.getConnectionDetails(url);
		JsonObject respObj = gson.fromJson(resp, JsonObject.class).getAsJsonObject();
		System.out.println("Starting get campaign validation");
		Validator.getCreativeValidation(respObj);
		JsonObject olderCreativeResponse = gson.fromJson(resp, JsonObject.class).get("data").getAsJsonObject();
		System.out.println("Old Campaign response is " + olderCreativeResponse);
		return olderCreativeResponse;
	}

	public JsonObject updateCreative(String accessToken, String creativeId)
			throws ClientProtocolException, IOException {

		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost httpPost;
		ArrayList<NameValuePair> postParameters;
		httpPost = new HttpPost(server + "/api/v1/creative/update?access_token=" + accessToken + "&env=qa");
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("creative_id", creativeId));
		postParameters.add(new BasicNameValuePair("audio",
				"[{\"type\":\"audio\",\"media_id\":\"tc_ops_g_abm01\",\"bitrate\":\"9600\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid_audio.mp3\",\"duration\":\"00:00:15:000\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-audio-start-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"}]"));
		postParameters.add(new BasicNameValuePair("banner_static",
				"[{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab01\",\"placement\":\"audioCompanion\",\"width\":\"300\",\"height\":\"250\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-300x250.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-audio-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-audio-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"},{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab02\",\"placement\":\"spotlight\",\"width\":\"320\",\"height\":\"86\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x86.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-spotlight-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-spotlight-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"},{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab03\",\"placement\":\"banner\",\"width\":\"320\",\"height\":\"50\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x50.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-banner-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-banner-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"},{\"type\":\"banner_static\",\"media_id\":\"tc_ops_g_ab04\",\"placement\":\"interstitial\",\"width\":\"320\",\"height\":\"480\",\"media_url\":\"http://s.saavn.com/ads/adengg/api/tc_ops1/ab_renault-kwid-320x480.jpg\",\"click_through_url\":\"com.amazon.mobile.shopping.web://www.amazon.in/gp/product/B00ZV9RDKK\",\"fallback_click_url\":\"http://www.amazon.in/gp/product/B00ZV9RDKK\",\"third_party_imp_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-idle-imp%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\",\"third_party_click_tracker\":\"http://pubads.g.doubleclick.net/gampad/adx?iu=/6714/SAAVNAndroid%26t=%26cat%3Dpixel-display-idle-clk%26sz=1x1%26mob=js%26c=%%CACHEBUSTER%%\"}]"));
		postParameters.add(new BasicNameValuePair("creative_name", "honda_test"));
		postParameters.add(new BasicNameValuePair("is_display_native", "false"));
		httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
		HttpResponse objResponse = httpClient.execute(httpPost);
		HttpEntity entity = objResponse.getEntity();
		String responseString = EntityUtils.toString(entity, "UTF-8");
		System.out.println(responseString);
		JsonObject updatedCreativeResponse = gson.fromJson(responseString, JsonObject.class);
		Validator.updateCreativeValidation(updatedCreativeResponse);
		System.out.println("Updated campaign response is " + updatedCreativeResponse);
		return updatedCreativeResponse;
	}

	public void verifyUpdateCreative(JsonObject olderCreativeResponse, JsonObject updatedCreativeResponse) {

		// System.out.println("Older campaign details " + olderCreativeResponse);
		// System.out.println("Updated campaing details " + updatedCreativeResponse);
		Assert.assertFalse(
				olderCreativeResponse.get("creative_name").equals(updatedCreativeResponse.get("creative_name")));
		Assert.assertFalse(olderCreativeResponse.get("audio").equals(updatedCreativeResponse.get("audio")));
		Assert.assertFalse(
				olderCreativeResponse.get("banner_static").equals(updatedCreativeResponse.get("banner_static")));
		System.out.println("Verified : values are different");
	}

	public void deleteCreative(String access_token, String creativeId) throws Exception {
		String url = server + "/api/v1/creative/delete?access_token=" + access_token + "&creative_id=" + creativeId
				+ "&env=qa";
		// String resp = GetConnection.getConnectionDetails(url);
		String resp = GetConnection.getConnectionDetailsPutRequest(url);
		System.out.println("SoftDelete: " + resp);
		// JsonObject respObj = gson.fromJson(resp,
		// JsonObject.class).get("data").getAsJsonObject();
		String status = gson.fromJson(resp, JsonObject.class).getAsJsonObject().get("status").getAsString();
		System.out.println(status);
		Assert.assertTrue(status.equals("success"));
		System.out.println("Campaign is deleted");
	}

	public void verifyDelete(String access_token, String creativeId) throws Exception {
		String url = server + "/api/v1/creative/get?access_token=" + access_token + "&creative_id=" + creativeId
				+ "&env=qa";
		String resp = GetConnection.getConnectionDetails(url);
		System.out.println("Deleted" + resp);
		String archiveStatus = gson.fromJson(resp, JsonObject.class).get("data").getAsJsonObject().get("status")
				.getAsString();
		// System.out.println(archiveStatus);
		Assert.assertTrue(archiveStatus.equals("archived"));
		String campaignResponse = gson.fromJson(resp, JsonObject.class).getAsJsonObject().get("status").getAsString();
		Assert.assertTrue(campaignResponse.equals("success"));
	}
}

/**
 * 
 */
package testCases;

import java.io.FileInputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * @author aswingokulachandran
 *
 */
public class RecommendationSetTests {

	String server = null;

	@Test(priority = 0, description = "Creates Recommendation Set")
	@Parameters({ "server" })
	public RecommendationSetTests(String server) {
		// TODO Auto-generated constructor stub
		System.out.println("yyo 1");
		this.server = server;
		System.out.println("Server: " + this.server);
		System.out.println("yyo 2");

		createRecoSet();
	}

	
	public void createRecoSet() {
		try {
			System.out.println("Inside createRecoSet");
			CloseableHttpClient client = HttpClientBuilder.create().build();
			StringBuilder url = new StringBuilder(this.server + "/recommendation_set/create?env=qa");
			HttpPost post = new HttpPost(url.toString());

			Properties props = new Properties();
			FileInputStream fs = new FileInputStream("src/main/java/properties/RecommendationSetCreate.properties");
			
			props.load(fs);
			
			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
			String format = URLEncoder.encode(props.getProperty("format"), "UTF-8");
			params.add(new BasicNameValuePair("format", format));
			
			String campaign_id = URLEncoder.encode(props.getProperty("campaign_id"), "UTF-8");
			params.add(new BasicNameValuePair("campaign_id", campaign_id));
			
			String start_date = URLEncoder.encode(props.getProperty("start_date"), "UTF-8");
			params.add(new BasicNameValuePair("start_date", start_date));
			
			String end_date = URLEncoder.encode(props.getProperty("end_date"), "UTF-8");
			params.add(new BasicNameValuePair("end_date", end_date));
			
			String recommendation_set_name = URLEncoder.encode("RecosSampleTest2", "UTF-8");
			params.add(new BasicNameValuePair("recommendation_set_name", recommendation_set_name));
			
			String frequency_cap = URLEncoder.encode(props.getProperty("frequency_cap"), "UTF-8");
			params.add(new BasicNameValuePair("frequency_cap", frequency_cap));
			
			String access_token = URLEncoder.encode(props.getProperty("access_token"), "UTF-8");
			params.add(new BasicNameValuePair("access_token", access_token));
			
			String daily_budget = URLEncoder.encode(props.getProperty("daily_budget"), "UTF-8");
			params.add(new BasicNameValuePair("daily_budget", daily_budget));
			
			String cost_per_unit = URLEncoder.encode(props.getProperty("cost_per_unit"), "UTF-8");
			params.add(new BasicNameValuePair("cost_per_unit", cost_per_unit));
			
			String[] geo_country = {"\"USA\"", "\"IN\""};
			params.add(new BasicNameValuePair("geo_country", Arrays.toString(geo_country)));
			
			String platforms[] = {"\"iphoneApp\""};
			params.add(new BasicNameValuePair("platforms", Arrays.toString(platforms)));
			
			post.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse response = client.execute(post);
			
			String responseStr = EntityUtils.toString(response.getEntity());
			System.out.println(responseStr);
			

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

package testCases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Properties;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import resources.*;

import resources.GenerateToken;

public class CampaignTests  {

	Gson gson = new Gson();
	Properties prop;
	String server;

	
	
	
	@Test 
	@Parameters ({"server"})
	public void campaignTests(String server) throws Exception {
		this.server=server;
		GenerateToken tokn = GenerateToken.getInstance();
	//	String accessToken = GenerateToken.generateToken();
		String accessToken = tokn.generateToken();
		prop = new Properties();
		FileInputStream fs = new FileInputStream(
				"src/main/java/resources/CampaignProperties.properties");
		prop.load(fs);
		String campaignName = URLEncoder.encode(prop.getProperty("campaignName"), "UTF-8");
		String platform = URLEncoder.encode(prop.getProperty("platform"), "UTF-8");
		String adProperty = URLEncoder.encode(prop.getProperty("ad_property"), "UTF-8");
		String externalOderId = URLEncoder.encode(prop.getProperty("external_order_id"), "UTF-8");
		String purchaseOrder = URLEncoder.encode(prop.getProperty("purchase_order"), "UTF-8");
		// String dailyBudget = URLEncoder.encode(prop.getProperty("daily_budget"),
		// "UTF-8");
		String notes = URLEncoder.encode(prop.getProperty("notes"), "UTF-8");
		// String traffickerId = URLEncoder.encode(prop.getProperty("traffickerId"),
		// "UTF-8");

		/*
		 * Calling the createCampaign mathod for getting the /campaign ID
		 */
		String campaignId = createCampaign( accessToken, campaignName, platform, adProperty, purchaseOrder,
				externalOderId, notes);
		System.out.println("/campaign ID is " + campaignId);
		/*
		 * Getting the /campaign details by passing the /campaign ID to getCampaignId
		 * method
		 */
		JsonObject olderCampaignResponse = getCampaign(accessToken, campaignId);

		/*
		 * Updating the /campaign
		 */
		JsonObject updatedCampaignResponse = updateCampaign(accessToken, campaignId);

		/*
		 * Checking if the repsonse differs for both the order ids
		 */
		verifyUpdateCampaign(olderCampaignResponse, updatedCampaignResponse);
		/*
		 * delete /campaign
		 */
		deleteCampaign(campaignId, accessToken);
		/*
		 * Verify delete /campaign, after deleting, checkint the response by calling the
		 * getCampaign method
		 */

		verifyDeleteCampaign(accessToken, campaignId);
		
		/*
		 * Verify delete /campaign, after deleting, checkint the response by calling the
		 * getCampaign method
		 */
		String [] statusList  = {"unarchive", "pause", "resume"};
	
		for(int i = 0; i<statusList.length;i++) {
			JsonObject updatedCampaignStatusResponse = updateCampaignStatus(accessToken, campaignId, statusList[i]); 
			System.out.println("order resp : " +olderCampaignResponse);
			System.out.println("upd resp : " +updatedCampaignStatusResponse);
			//verifyUpdateStatus(olderCampaignResponse, updatedCampaignStatusResponse);
		}
		
		 

		
		
	}
	

	public String createCampaign( String accessToken, String campaignName, String platform, String adProperty,
			String purchaseOrder, String externalOrderID, String notes) throws Exception {
		

		System.out.println("-------Create campaign------");

		
		String url = this.server+"/campaign/create?&access_token=" + accessToken + "&campaign_name="
				+ campaignName + "&platform=" + platform + "&ad_property=" + adProperty + "&external_order_id="
				+ externalOrderID + "&purchase_order=" + purchaseOrder + "&notes=" + notes + "&env=qa";
		
		
		System.out.println("URL is" +url);
		URL con= new URL(url);
		HttpURLConnection httpCon =  (HttpURLConnection) con.openConnection();
		httpCon.setRequestMethod("POST");
		BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
		String inputLine;
		String resp="";
		while((inputLine=in.readLine())!=null) {
			resp=resp.concat(inputLine);
		}

		JsonObject respObject = gson.fromJson(resp, JsonObject.class);
		// Starting validation here
		System.out.println("Starting create /campaign validation");
		Validator.createCampaignValidation(respObject);
		String campaignId = respObject.get("data").getAsJsonObject().get("campaign_id").getAsString();
		System.out.println("/campaign is created, ID is " + campaignId);
		return campaignId;

	}

	public JsonObject getCampaign( String accessToken, String campaignId) throws Exception {
		
		System.out.println("-------Get campaign------ \n");

		
		String url = this.server+"/campaign/get&access_token=" + accessToken + "&campaign_id="
				+ campaignId + "&env=qa";
		String resp = GetConnection.getConnectionDetails(url);
		JsonObject respObj = gson.fromJson(resp, JsonObject.class).getAsJsonObject();
		System.out.println("Starting get campaign validation");
		Validator.getCampaignValidation(respObj);
		JsonObject olderCampaignResponse = gson.fromJson(resp, JsonObject.class).get("data").getAsJsonObject();
		System.out.println("campaign response is " + olderCampaignResponse);
		return olderCampaignResponse;

	}

	public JsonObject updateCampaign(String accessToken, String campaignId) throws Exception {

		System.out.println("-------Update campaign------ \n");
		
		/*
		 * Updating the /campaign by changing the values of all the fields
		 */
		String campaignName = URLEncoder.encode(prop.getProperty("UcampaignName"), "UTF-8");
		String platform = URLEncoder.encode(prop.getProperty("Uplatform"), "UTF-8");
		String adProperty = URLEncoder.encode(prop.getProperty("Uad_property"), "UTF-8");
		String externalOderId = URLEncoder.encode(prop.getProperty("Uexternal_order_id"), "UTF-8");
		String purchaseOrder = URLEncoder.encode(prop.getProperty("Upurchase_order"), "UTF-8");
		// dailyBudget = URLEncoder.encode(prop.getProperty("Udaily_budget"), "UTF-8");
		String notes = URLEncoder.encode(prop.getProperty("Unotes"), "UTF-8");
		String traffickerId = URLEncoder.encode(prop.getProperty("UtraffickerId"), "UTF-8");

		String url = this.server+"/campaign/update?access_token=" + accessToken + "&campaign_id="
				+ campaignId + "&campaign_name=" + campaignName + "&platform=" + platform + "&ad_property=" + adProperty
				+ "&purchase_order=" + purchaseOrder + "&external_order_id=" + externalOderId + "&notes=" + notes
				+ "&trafficker_id=" + traffickerId + "&env=qa";
		String resp = GetConnection.getConnectionDetailsPostRequest(url);
		JsonObject respObj = gson.fromJson(resp, JsonObject.class);
		System.out.println("update3 json " +resp);
		// Validating the keys
		Validator.updateCampaignValidation(respObj);
		System.out.println(resp);
		JsonObject updatedCampaignResponse = gson.fromJson(resp, JsonObject.class).get("data").getAsJsonObject();
		System.out.println("campaign is updated!");
		return updatedCampaignResponse;
	}
	
	

	public void verifyUpdateCampaign(JsonObject olderCampaignResponse, JsonObject updatedCampaignResponse)
			throws JsonParseException, IOException {
		
		System.out.println("-------Verify Update campaign------ \n");

		System.out.println("Older campaign details " + olderCampaignResponse);
		System.out.println("Updated campaing details " + updatedCampaignResponse);

		AssertJUnit.assertFalse(
				olderCampaignResponse.get("campaign_name").equals(updatedCampaignResponse.get("campaign_name")));
		AssertJUnit.assertFalse(olderCampaignResponse.get("platform").equals(updatedCampaignResponse.get("platform")));
		AssertJUnit.assertFalse(olderCampaignResponse.get("external_order_id")
				.equals(updatedCampaignResponse.get("external_order_id")));
		AssertJUnit.assertFalse(
				olderCampaignResponse.get("purchase_order").equals(updatedCampaignResponse.get("purchase_order")));
		AssertJUnit.assertFalse(olderCampaignResponse.get("notes").equals(updatedCampaignResponse.get("notes")));

		System.out.println("Verified : values are different");

	}

	public void deleteCampaign(String campaignId, String accessToken) throws Exception {
		
		System.out.println("-------Delete campaign------ \n");
		
		String url = this.server+"/campaign/delete?campaign_id=" + campaignId + "&access_token="
				+ accessToken + "&env=qa";
		String resp = GetConnection.getConnectionDetailsPutRequest(url);
		System.out.println("SoftDelete: " + resp);
		// JsonObject respObj = gson.fromJson(resp,
		// JsonObject.class).get("data").getAsJsonObject();
		String status = gson.fromJson(resp, JsonObject.class).getAsJsonObject().get("status").getAsString();
		System.out.println(status);
		AssertJUnit.assertTrue(status.equals("success"));
		System.out.println("campaign is deleted");
	}

	public void verifyDeleteCampaign(String accessToken, String campaignId) throws Exception {
		
		System.out.println("-------Verify Delete campaign------ \n");
		
		String url = this.server+"/campaign/get&access_token=" + accessToken + "&campaign_id="
				+ campaignId + "&env=qa";
		String resp = GetConnection.getConnectionDetails(url);
		System.out.println("Deleted" + resp);
		String archiveStatus = gson.fromJson(resp, JsonObject.class).get("data").getAsJsonObject().get("status").getAsString();
		System.out.println("archive status is " +archiveStatus);
		AssertJUnit.assertTrue(archiveStatus.equals("archived"));
		String campaignResponse = gson.fromJson(resp, JsonObject.class).getAsJsonObject().get("status").getAsString();
		AssertJUnit.assertTrue(campaignResponse.equals("success"));

	}
	
	public JsonObject updateCampaignStatus(String accessToken, String campaignId ,String status) throws IOException {
		
		System.out.println("-------Update status field campaign------ \n");
		
		/*
		 * Updating the /campaign by changing the values of all the fields
		 */
		String campaignName = URLEncoder.encode(prop.getProperty("UcampaignName"), "UTF-8");
		String platform = URLEncoder.encode(prop.getProperty("Uplatform"), "UTF-8");
		String adProperty = URLEncoder.encode(prop.getProperty("Uad_property"), "UTF-8");
		String externalOderId = URLEncoder.encode(prop.getProperty("Uexternal_order_id"), "UTF-8");
		String purchaseOrder = URLEncoder.encode(prop.getProperty("Upurchase_order"), "UTF-8");
		// dailyBudget = URLEncoder.encode(prop.getProperty("Udaily_budget"), "UTF-8");
		String notes = URLEncoder.encode(prop.getProperty("Unotes"), "UTF-8");
		String traffickerId = URLEncoder.encode(prop.getProperty("UtraffickerId"), "UTF-8");

		String url = this.server+"/campaign/update?access_token=" + accessToken + "&campaign_id="
				+ campaignId + "&env=qa" +"&status="+status;
		
		System.out.println("upd status url" +url);
		String resp = GetConnection.getConnectionDetailsPostRequest(url);
		JsonObject respObj = gson.fromJson(resp, JsonObject.class);
		System.out.println("update4 json " +resp);
		// Validating the keys
		Validator.updateCampaignValidation(respObj);
		System.out.println(resp);
		JsonObject updatedCampaignStatusResponse = gson.fromJson(resp, JsonObject.class).get("data").getAsJsonObject();
		System.out.println("campaign status is updated!");
		return updatedCampaignStatusResponse;
		
		
		
	}

}

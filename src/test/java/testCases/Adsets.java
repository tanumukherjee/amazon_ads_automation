/**
 * 
 */
package testCases;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import resources.GenerateToken;
import resources.Validator;

/**
 * @author aswin
 *
 */

public class Adsets {

	String accessToken = GenerateToken.generateToken();
	String server;

	@Test
	@Parameters ({"server"})
	public void adsetsTests(String server) {
		this.server=server;
		
		String ad_set_id = adsetCreate();
		adsetGet(ad_set_id);

		adsetUpdate(ad_set_id);
		adsetDelete(ad_set_id);
		negativeTests();
//		negativeCreate();
//		negativeGet();
//		negativeUpdate();
//		negativeDelete();

		negativeTestsForParams();
	}

	/*
	 * AD SET CREATE
	 */
	public String adsetCreate() {
		try {

			String[] platform = { "\"Android\"" };
			String[] day_targeting = {"\"thursday\"", "\"friday\""};
			String[] country = {"\"India\""};
			String[] state = {"\"California\""};
			String[] city = {"\"Tehran\"", "\"Paris\""};

			HttpClient client = HttpClientBuilder.create().build();
			String url = server+"/adset/create?access_token=" + this.accessToken + "&env=qa";

			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new BasicNameValuePair("ad_set_name", "AswinAdSet"));
			params.add(new BasicNameValuePair("campaign_id", "2396702146"));
			params.add(new BasicNameValuePair("end_date", "2020-05-29T00:00:00-05:30"));
			params.add(new BasicNameValuePair("start_date", "2020-03-03T00:00:00-05:30"));
			params.add(new BasicNameValuePair("advertiser", "saavn"));
			params.add(new BasicNameValuePair("cost_per_unit", "500"));
			params.add(new BasicNameValuePair("daily_budget", "10"));
			params.add(new BasicNameValuePair("platform", java.util.Arrays.toString(platform)));
			params.add(new BasicNameValuePair("creative_id", "5bab503377b60"));
			params.add(new BasicNameValuePair("day_targeting", java.util.Arrays.toString(day_targeting)));
			params.add(new BasicNameValuePair("geo_country", java.util.Arrays.toString(country)));
			params.add(new BasicNameValuePair("geo_state", java.util.Arrays.toString(state)));
			params.add(new BasicNameValuePair("geo_city", java.util.Arrays.toString(city)));
			params.add(new BasicNameValuePair("click_macro", "Test AdSet QA"));
			params.add(new BasicNameValuePair("comment", "Random QA comment"));

			System.out.println("url: " + url);
			HttpPost post = new HttpPost(url);
			post.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse res = client.execute(post);

			System.out.println("> Create Adset Response2");
			String resStr = EntityUtils.toString(res.getEntity());
			System.out.println(resStr);
			System.out.println("AdSet create Validation starts....");
			JsonObject resJson = new Gson().fromJson(resStr, JsonObject.class);
			Validator.adSetCreateValidation(resJson);
			System.out.println("End of AdSet Create Validation");

			if(resJson.has("data")){
				JsonObject data = resJson.get("data").getAsJsonObject();
				if(data.has("ad_set_id")){
					String adsetId = data.get("ad_set_id").getAsString();
					return adsetId;
				}
			}

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * AD SET GET
	 */

	public String adsetGet(String ad_set_id) {
		try {
			HttpClient client = HttpClientBuilder.create().build();
			int detail = 1;
			String url = server+"/adset/get?access_token=" + this.accessToken + "&ad_set_id="
					+ ad_set_id + "&env=qa&detail_level"+detail;

			HttpGet get = new HttpGet(url);
			System.out.println("URL: " + url);
			HttpResponse res = client.execute(get);
			String resStr = EntityUtils.toString(res.getEntity());
			System.out.println(resStr);
			JsonObject respObj = new Gson().fromJson(resStr, JsonObject.class);
			Validator.adSetGetValidation(respObj, detail);

			System.out.println("==============================================");
			System.out.println(">>>>End of Adset GET");
			System.out.println("==============================================");
			return resStr;
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	/*
	 * AD SET DELETE
	 */

	public void adsetDelete(String ad_set_id) {
		System.out.println(">>> Delete Adset");
		try {
			HttpClient client = HttpClientBuilder.create().build();
			String url = server+"/adset/delete?access_token=" + this.accessToken + "&ad_set_id="
					+ ad_set_id + "&env=qa";
			System.out.println("URL: " + url );
			HttpPut delete = new HttpPut(url);

			HttpResponse res;

			res = client.execute(delete);
			String resStr = EntityUtils.toString(res.getEntity());
			System.out.println(resStr);

			adsetGet(ad_set_id);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * AD SET UPDATE
	 */
	public void adsetUpdate(String ad_set_id) {
		try {

			String[] platform = { "\"Android\"" };
			String[] day_targeting = {"\"monday\""};
			String[] country = {"\"India\""};
			String[] state = {"\"New York\""};
			String[] city = {"\"Berlin\"", "\"Paris\""};

			System.out.println("> Original adSet");
			String orig1 = adsetGet(ad_set_id);

			Random random = new Random();
			int val = random.nextInt(200000000);

			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

			// params.add(new BasicNameValuePair("ad_set_id", ad_set_id));
			params.add(new BasicNameValuePair("ad_set_name", "bohemian rhapsody"));
			params.add(new BasicNameValuePair("end_date", "2020-10-10T00:00:00-05:30"));
			params.add(new BasicNameValuePair("start_date", "2020-03-03T00:00:00-05:30"));
//			params.add(new BasicNameValuePair("campaign_id", "2245671012"));
			params.add(new BasicNameValuePair("campaign_id", "2245671047"));
			//params.add(new BasicNameValuePair("advertiser", "saavn"));
			//params.add(new BasicNameValuePair("cost_per_unit", "50"));
			params.add(new BasicNameValuePair("daily_budget", "1"));
			params.add(new BasicNameValuePair("platform", java.util.Arrays.toString(platform)));
			params.add(new BasicNameValuePair("creative_id", "5bab503377b60"));
			params.add(new BasicNameValuePair("day_targeting", java.util.Arrays.toString(day_targeting)));
			params.add(new BasicNameValuePair("geo_country", java.util.Arrays.toString(country)));
			params.add(new BasicNameValuePair("geo_state", java.util.Arrays.toString(state)));
			params.add(new BasicNameValuePair("geo_city", java.util.Arrays.toString(city)));
			params.add(new BasicNameValuePair("click_macro", "Test AdSet Update QA"));
			params.add(new BasicNameValuePair("comment", "Random QA comment for update"));

			HttpClient client = HttpClientBuilder.create().build();
			String url = server+"/adset/update?access_token=" + this.accessToken + "&ad_set_id="
					+ ad_set_id + "&env=qa";

			System.out.println("URL: " + url);
			HttpPost put = new HttpPost(url);
			put.setEntity(new UrlEncodedFormEntity(params));
			System.out.println("PUT to string:.....");
			System.out.println(put.toString());

			HttpPost post = new HttpPost(url);
			post.setEntity(new UrlEncodedFormEntity(params));

			HttpResponse res = client.execute(post);
			System.out.println("> Update Adset Response");
			String resStr = EntityUtils.toString(res.getEntity());
			System.out.println(resStr);

			System.out.println("==============================================");
			System.out.println(">>>>End of Adset Update");
			System.out.println("==============================================");

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void negativeTests() {

		String apiToken = GenerateToken.generateToken("SaavnAPITestCLient", "SaavnAPITestPass");
		String testToken = GenerateToken.generateToken("testclient", "testpass");

		this.accessToken = testToken;
		String adsetId = adsetCreate();
		System.out.println("adsetId: " + adsetId);
		System.out.println("Adset created using testToken");

		System.out.println("-------------------");
		System.out.println("Segment accessed using apiToken");
		this.accessToken = apiToken;
		adsetGet(adsetId);
		this.accessToken = testToken;

	}

	public void negativeCreate() {
		try {

			String[] platform = { "\"Android\"" };
			HttpClient client = HttpClientBuilder.create().build();
			String url =server+"/adset/create?access_token=" + this.accessToken + "&env=qa";

			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new BasicNameValuePair("ad_set_name", "NEGATIVE"));
			params.add(new BasicNameValuePair("campaign_id", "jkndkjasndjkna"));
			params.add(new BasicNameValuePair("end_date", "2020-10-10T00:00:00-05:30"));
			params.add(new BasicNameValuePair("start_date", "2020-03-03T00:00:00-05:30"));
		//	params.add(new BasicNameValuePair("cost_per_unit", "hjg"));
			params.add(new BasicNameValuePair("daily_budget", "mnb"));
			params.add(new BasicNameValuePair("platform", java.util.Arrays.toString(platform)));
			params.add(new BasicNameValuePair("creative_id", "5a8401746ea7a"));

			HttpPost post = new HttpPost(url);
			post.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse res = client.execute(post);

			System.out.println("> Create Adset Response3");
			String resStr = EntityUtils.toString(res.getEntity());
			System.out.println(resStr);

			JsonObject resJson = new Gson().fromJson(resStr, JsonObject.class);
			if (resJson.has("data")) {
				JsonObject data = resJson.get("data").getAsJsonObject();
				if (data.has("ad_set_id")) {
					String ad_set_id = data.get("ad_set_id").getAsString();
					System.out.println("==============================================");
					System.out.println(">>>>End of Adset Create");
					System.out.println("==============================================");
				}
			}

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void negativeGet() {
		try {
			HttpClient client = HttpClientBuilder.create().build();
			String url = server+"/adset/get?access_token=" + this.accessToken + "&env=qa&ad_set_id=" ;

			HttpGet get = new HttpGet(url);

			HttpResponse res = client.execute(get);
			String resStr = EntityUtils.toString(res.getEntity());
			System.out.println("Adset Get Response: " + resStr);

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void negativeUpdate() {
		try {

			Random random = new Random();
			int val = random.nextInt(200000000);

			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

			// params.add(new BasicNameValuePair("ad_set_id", ad_set_id));
			params.add(new BasicNameValuePair("ad_set_name", Integer.toString(val)));
			params.add(new BasicNameValuePair("end_date", "2020-10-10T00:00:00-05:30"));
			params.add(new BasicNameValuePair("start_date", "2020-03-03T00:00:00-05:30"));
			params.add(new BasicNameValuePair("campaign_id", "2245671012"));

			HttpClient client = HttpClientBuilder.create().build();
			String url = server+"/adset/update?access_token=" + this.accessToken + "&env=qa&ad_set_id=";

			System.out.println("URL: " + url);
			HttpPut put = new HttpPut(url);
			put.setEntity(new UrlEncodedFormEntity(params));
			System.out.println("PUT to string:.....");
			System.out.println(put.toString());

			HttpPost post = new HttpPost(url);
			post.setEntity(new UrlEncodedFormEntity(params));

			HttpResponse res = client.execute(post);
			System.out.println("> Update Adset Response");
			String resStr = EntityUtils.toString(res.getEntity());
			System.out.println(resStr);

			System.out.println("==============================================");
			System.out.println(">>>>End of Adset Update");
			System.out.println("==============================================");

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void negativeDelete() {
		String adsetId = adsetCreate();
		adsetDelete(adsetId);
		adsetDelete(adsetId);
	}

	public void checkAdsetPostDel() {
		try {

			CreativeTests cre = new CreativeTests();

			String creativeId = cre.createCreative(accessToken);

			// create adset with the above creative id

		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void negativeTestsForParams(){

		String[] platform = { "\"Android\"" };
		String[] day_targeting = {"\"thursday\"", "\"friday\""};
		String[] country = {"\"India\""};
		String[] state = {"\"California\""};
		String[] city = {"\"Tehran\"", "\"Paris\""};

		HttpClient client = HttpClientBuilder.create().build();
		String url = server+"/adset/create?access_token=" + this.accessToken + "&env=qa";

		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

		params.add(new BasicNameValuePair("ad_set_name", "AswinAdSet"));
		params.add(new BasicNameValuePair("campaign_id", "2396702146"));
		params.add(new BasicNameValuePair("end_date", "2020-10-10T00:00:00-05:30"));
		params.add(new BasicNameValuePair("start_date", "2020-03-03T00:00:00-05:30"));
		params.add(new BasicNameValuePair("cost_per_unit", "500"));
		params.add(new BasicNameValuePair("daily_budget", "10"));
		params.add(new BasicNameValuePair("platform", java.util.Arrays.toString(platform)));
		params.add(new BasicNameValuePair("creative_id", "5bab503377b60"));
		params.add(new BasicNameValuePair("day_targeting", "[\"sunday\"]"));
		params.add(new BasicNameValuePair("geo_country", java.util.Arrays.toString(country)));
		params.add(new BasicNameValuePair("geo_state", java.util.Arrays.toString(state)));
		params.add(new BasicNameValuePair("geo_city", java.util.Arrays.toString(city)));
		params.add(new BasicNameValuePair("click_macro", "Test AdSet QA"));
		params.add(new BasicNameValuePair("comment", "Random QA comment"));

		System.out.println("url: " + url);
		HttpPost post = new HttpPost(url);
		try{
			post.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse res = client.execute(post);
			System.out.println("> Create Adset Response4");
			String resStr = EntityUtils.toString(res.getEntity());
			System.out.println(resStr);
		}catch(Exception e){
			e.printStackTrace();
		}



	}

}
